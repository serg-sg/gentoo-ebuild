# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg unpacker

DESCRIPTION="MyOffice Standard. Home Edition is a set of office applications for creating text and spreadsheet documents."
HOMEPAGE="https://myoffice.ru/products/standard-home-edition/"
LICENSE="MyOffice"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist mirror strip"

if [[ ${PN} == myoffice-standard-home-edition ]] ; then
	MY_PN="myoffice-standard-home-edition"
fi

SRC_URI="https://preset.myoffice-app.ru/${MY_PN}_${PV}_amd64.deb"

RDEPEND="
	>=sys-libs/glibc-2.17
"

S=${WORKDIR}

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	# Перемещаем mime
	path=usr/share/mime/application
	mkdir -p ${path}
	mv "${S}/usr/local/bin/${MY_PN}/"*.xml ${path} || die

	# Перемещаем ярлыки приложений
	path=usr/share/applications
	mkdir -p ${path}
	mv "${S}/usr/local/bin/${MY_PN}/"*.desktop ${path} || die

	# Исправляем строки запуска в ярлыках запуска
	sed -i -E -e "s/ %u$/.sh %u\nPath=\/opt\/${MY_PN}/" ${path}/myoffice*.desktop
	sed -i -E -e "s/^Exec=myoffice/Exec=\/opt\/${MY_PN}\/myoffice/" ${path}/myoffice*.desktop
	echo Path=/opt/${MY_PN} >> ${path}/myoffice*.desktop

	# Перемещаем иконки приложений в формате svg
	path=usr/share/icons/hicolor/scalable/apps
	mkdir -p ${path}
	mv "${S}/usr/local/bin/${MY_PN}/icons/app_text_icon.svg" ${path}/myoffice-text-home-edition.svg || die
	mv "${S}/usr/local/bin/${MY_PN}/icons/app_sheet_icon.svg" ${path}/myoffice-spreadsheet-home-edition.svg || die

	#fonts
	IFW_FONTS_DEST_DIR="usr/share/fonts/myoffice/"
	mkdir -p ${IFW_FONTS_DEST_DIR}
	cp "${S}/usr/local/bin/${MY_PN}/Fonts"/* ${IFW_FONTS_DEST_DIR} || die

	APPS="text sheet"
	for app in ${APPS}; do
		for icon in "${S}/usr/local/bin/${MY_PN}/icons/app_${app}_icon_"*.png; do
			size="${icon##*/app_${app}_icon_}"
			size=${size%.png}
			mkdir -p "usr/share/icons/hicolor/${size}x${size}/apps"
			if [ ${app} = "text" ]; then
				cp ${icon} "usr/share/icons/hicolor/${size}x${size}/apps/myoffice-${app}-home-edition.png" || die
			elif [ ${app} = "sheet" ]; then
				cp ${icon} "usr/share/icons/hicolor/${size}x${size}/apps/myoffice-spread${app}-home-edition.png" || die
			fi
		done
	done

	# Перемещаем в opt
	mkdir -p "${S}/opt"
	mv "${S}/usr/local/bin/${MY_PN}" "${S}/opt/" || die

	default
}

src_install() {

	mv * "${D}" || die

}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
	vxdg_icon_cache_update

}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
