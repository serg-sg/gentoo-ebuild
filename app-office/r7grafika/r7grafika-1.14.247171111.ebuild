# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit unpacker xdg

DESCRIPTION="Создание блок-схем и диаграмм"
HOMEPAGE="https://r7-office.ru/grafika"
LICENSE="Apache-2.0"
SLOT="0"

KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist strip mirror"

MY_PV=$(ver_rs 3- '')
MY_PN="r7draw-desktop"

SRC_URI="
	amd64? ( https://download.r7-office.ru/r7graph/${MY_PN}-amd64-${MY_PV}.deb )
"

RDEPEND="
	x11-libs/gtk+:3
	x11-libs/libnotify
	dev-libs/nss
	x11-libs/libXScrnSaver
	x11-libs/libXtst
	x11-misc/xdg-utils
	app-accessibility/at-spi2-core
	sys-apps/util-linux
	app-crypt/libsecret
	dev-libs/libappindicator
"

S=${WORKDIR}

src_unpack() {
	unpack_deb ${A}
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	if type update-alternatives 2>/dev/null >&1; then
		# Remove previous link if it doesn't use update-alternatives
		if [ -L '/usr/bin/r7draw-desktop' -a -e '/usr/bin/r7draw-desktop' -a "`readlink '/usr/bin/r7draw-desktop'`" \
			!= '/etc/alternatives/r7draw-desktop' ]; then
		rm -f '/usr/bin/R7Grafika'
		fi
		update-alternatives --install '/usr/bin/r7draw-desktop' 'r7draw-desktop' '/opt/Р7-Графика/r7draw-desktop' 100 \
			|| ln -sf '/opt/Р7-Графика/r7draw-desktop' '/usr/bin/r7draw-desktop'
	else
		ln -sf '/opt/Р7-Графика/r7draw-desktop' '/usr/bin/r7draw-desktop'
	fi

	# SUID chrome-sandbox for Electron 5+
	chmod 4755 '/opt/Р7-Графика/chrome-sandbox' || true

	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		# Delete the link to the binary
		if type update-alternatives >/dev/null 2>&1; then
			update-alternatives --remove 'r7draw-desktop' '/usr/bin/r7draw-desktop'
		else
			rm -f '/usr/bin/r7draw-desktop'
		fi
	fi
	xdg_desktop_database_update
}
