# Copyright 2020-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit unpacker
# pax-utils xdg

DESCRIPTION="Offline help for ONLYOFFICE Desktop Editors. This package contains offline help files"
HOMEPAGE="https://www.onlyoffice.com/"

MY_P="onlyoffice"
OPT="opt/${MY_P}"

SRC_URI="https://github.com/ONLYOFFICE/DesktopEditors/releases/download/v"${PV}"/${MY_P}-desktopeditors-help_all.deb -> "${P}"_all.deb"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="bindist strip mirror"

RDEPEND="
	!app-office/onlyoffice-help

	|| (
		~app-office/onlyoffice-${PV}
		~app-office/onlyoffice-bin-${PV}
	)
"

S=${WORKDIR}

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	gzip -d usr/share/doc/${MY_P}-desktopeditors-help/changelog.Debian.gz || die
	mkdir -p usr/share/doc/${PF}
	mv usr/share/doc/${MY_P}-desktopeditors-help/changelog.Debian usr/share/doc/${PF}/changelog || die
	mv usr/share/doc/${MY_P}-desktopeditors-help/copyright usr/share/doc/${PF}/copyright || die
	rm -rf usr/share/doc/${MY_P}-desktopeditors-help

	default
}

src_install() {
	mv * "${D}" || die
}
