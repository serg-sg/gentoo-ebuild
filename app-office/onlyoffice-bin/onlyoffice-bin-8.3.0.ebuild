# Copyright 2020-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop unpacker xdg

DESCRIPTION="ONLYOFFICE Desktop Editors is a free office suite that combines text, spreadsheet and presentation editors allowing to create, view and edit documents without an Internet connection. It is fully compatible with Office Open XML formats: .docx, .xlsx, .pptx"

HOMEPAGE="https://www.onlyoffice.com/"
SRC_URI="
	amd64? (
		https://github.com/ONLYOFFICE/DesktopEditors/releases/download/v"${PV}"/onlyoffice-desktopeditors_amd64.deb \
			-> "${P}"_amd64.deb
	)
"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="help"
RESTRICT="bindist strip mirror"

RDEPEND="
	!app-office/onlyoffice

	|| (
		net-misc/curl
		net-misc/wget
	)

	media-plugins/gst-plugins-libav
	media-libs/alsa-lib
	media-libs/gst-plugins-ugly

	dev-libs/atk
	dev-util/desktop-file-utils
	sys-devel/gcc
	x11-base/xorg-server

	x11-libs/gtk+:3
	x11-libs/cairo
	x11-libs/libxkbcommon[X(+)]
	x11-libs/libXScrnSaver
	x11-misc/xdg-utils

	media-fonts/dejavu
	media-fonts/liberation-fonts
	media-fonts/crosextrafonts-carlito
	media-fonts/corefonts
	media-fonts/takao-fonts

	help? ( app-office/onlyoffice-help-bin )

	|| (
		media-libs/libpulse
		media-sound/apulse
	)
"

S=${WORKDIR}
MY_P="onlyoffice"
OPT="opt/${MY_P}"

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	gzip -d usr/share/doc/${MY_P}-desktopeditors/changelog.Debian.gz || die
	mkdir -p usr/share/doc/${PF}
	mv usr/share/doc/${MY_P}-desktopeditors/changelog.Debian usr/share/doc/${PF}/changelog || die
	mv usr/share/doc/${MY_P}-desktopeditors/copyright usr/share/doc/${PF}/copyright || die
	rm -rf usr/share/doc/${MY_P}-desktopeditors

	default

	# Разрешить запуск ONLYOFFICE на ALSA-системах через
	# media-sound/apulse
	sed -i -e 's|\(export LD_LIBRARY_PATH=$DIR$LDLPATH\)|\1:'"${EPREFIX}"'/usr/'$(get_libdir)'/apulse|' \
		"${S}"/usr/bin/onlyoffice-desktopeditors || die
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
