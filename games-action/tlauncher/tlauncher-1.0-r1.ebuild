# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit unpacker xdg

DESCRIPTION="TLauncher – Лучший лаунчер Майнкрафт"
HOMEPAGE="https://tlauncher.org/"
LICENSE="GPL3-0"
SLOT="0"

KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist strip mirror"
MY_PV=$(ver_rs 1- '')
MY_PN="TLauncher"

SRC_URI="
	https://dl2.tlauncher.org/f.php?f=files%2F${MY_PN}.v${MY_PV}.zip -> ${P}.zip
"

RDEPEND="
	app-admin/sudo
	|| ( >=dev-java/openjdk-8 >=dev-java/openjdk-bin-8 )
	>=dev-java/openjdk-jre-bin-8
"
#	dev-java/openjfx:8

QA_PREBUILT="*"
S=${WORKDIR}

src_unpack() {
	default
}

src_prepare() {
	mkdir -p opt/${PN} || die
	mv ${MY_PN}.v${MY_PV}/${MY_PN}.jar opt/${PN} || die
	chown -R :games opt/${PN} || die

	# Удалить лишнее от распаковки
	rm -rf ${MY_PN}.v${MY_PV} || die

	# Ярлык для меню Пуск
	mkdir -p usr/share/applications/ || die
	cp ${FILESDIR}/tlauncher.desktop usr/share/applications/ || die

	# Иконка для ярлыка в меню Пуск
	mkdir -p usr/share/icons/hicolor/scalable/apps || die
	cp ${FILESDIR}/tlauncher.svg usr/share/icons/hicolor/scalable/apps/ || die

	default
}

src_install() {
	mv * "${D}" || die

	fperms a+x "${EPREFIX}/opt/${PN}/"
}

pkg_postinst() {

	xdg_desktop_database_update
}

pkg_postrm() {

	xdg_desktop_database_update
}
