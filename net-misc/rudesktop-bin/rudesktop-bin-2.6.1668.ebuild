# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

PYTHON_COMPAT=( python3_{10..13} )
inherit xdg unpacker

# Убираем суффикс в названии продукта
MY_PN=${PN/-bin/}

DESCRIPTION="A remote control software."
HOMEPAGE="https://rudesktop.ru/"
SRC_URI="
	amd64? (
		https://${MY_PN}.ru/download/${MY_PN}-${ARCH}.deb -> ${P}.deb
	)
"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"

# Зависимости для запуска этой программы
RDEPEND="
	app-misc/ca-certificates
	app-admin/sudo
	dev-libs/libayatana-appindicator
	media-libs/alsa-lib
	media-libs/libpulse
	sys-apps/systemd-utils
	x11-libs/gtk+:3
	x11-libs/libxcb
	x11-libs/libXfixes
	x11-libs/libXtst
	${PYTHON_DEPS}
"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RESTRICT="bindist mirror strip"

S=${WORKDIR}

src_unpack() {
	unpack_deb ${A}
}

src_install() {
	mv * "${D}" || die

	# Перемещаем библиотеку в подпапку с именем пакета
	mkdir -p "${D}/usr/lib/${MY_PN}"
	mv "${D}/usr/lib/libsciter-gtk.so" "${D}/usr/lib/${MY_PN}/"

	# Добавление службы rudesktop
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')

	if [ "systemd" == "${INITSYS}" ]; then
		mkdir -p "${D}/usr/lib/systemd/system"
		cp "${D}/usr/share/${MY_PN}-client/files/systemd/${MY_PN}.service" "${D}/usr/lib/systemd/system/${MY_PN}.service"
	else
		mkdir -p "${D}/etc/init.d/"
		echo '#!/sbin/openrc-run
name="RuDesktop"
description="RuDesktop Daemon Service"
supervisor="supervise-daemon"
command="/usr/bin/rudesktop"
command_args="--service"
command_user="root"
pidfile="/run/rudesktop"

depend() {
	after xdm
	need net
}' > "${D}/etc/init.d/${MY_PN}"

	# Исправление прав на запуск службы
	chmod +x "${D}/etc/init.d/${MY_PN}"

	fi
}

pkg_preinst() {
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')
	if [ "systemd" == "${INITSYS}" ]; then
		service ${MY_PN} stop || true

		if [ -e /etc/systemd/system/${MY_PN}.service ]; then
			rm -f "/etc/systemd/system/${MY_PN}.service" "/usr/lib/systemd/system/${MY_PN}.service" \
				"/usr/lib/systemd/user/${MY_PN}.service"
		fi
	else
		rc-service ${MY_PN} stop
	fi
}

pkg_postinst() {
	echo "root ALL=(ALL) NOPASSWD:SETENV:/usr/bin/${MY_PN}" > /etc/sudoers.d/${MY_PN}

	/usr/bin/python3 /usr/share/${MY_PN}-client/files/patch-X11.py
	# Добавление и запуск службы в автозагрузку
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')
	if [ "systemd" == "${INITSYS}" ]; then
		systemctl daemon-reload
		systemctl enable ${MY_PN}
		systemctl start ${MY_PN}
	else
		# Исправление прав на запуск службы
		chmod +x "/etc/init.d/${MY_PN}"

		rc-update add ${MY_PN} default
		rc-service ${MY_PN} start
	fi

	if [ ! -z "$RUDESKTOP_DOMAIN" ]; then
		/usr/bin/${MY_PN} --rendezvous $RUDESKTOP_DOMAIN || true
	fi

	mv "/usr/lib/${MY_PN}/libsciter-gtk.so" /usr/lib/
	rm -rf "/usr/lib/${MY_PN}"
}

pkg_prerm() {
	# Зачистка мусора
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')
	if [ "systemd" == "${INITSYS}" ]; then
		systemctl stop ${MY_PN} || true
		systemctl disable ${MY_PN} || true
		rm -f "/etc/systemd/system/${MY_PN}.service" "/usr/lib/systemd/system/${MY_PN}.service" \
			"/usr/lib/systemd/user/${MY_PN}.service /etc/sudoers.d/${MY_PN}"
	else
		rc-update delete ${MY_PN}
		rc-service ${MY_PN} stop
	fi
	rm -rf /usr/lib/libsciter-gtk.so || die
	rm -rf "/usr/share/${MY_PN}-client" || die
	rm -rf "/etc/sudoers.d/${MY_PN}" || die
	rm -rf /root/.config/rudesktop || die
}
