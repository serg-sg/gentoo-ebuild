# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit chromium-2 desktop unpacker pax-utils xdg

DESCRIPTION="Chromium browser with GOST cryptography support"
HOMEPAGE="https://github.com/deemru/chromium-gost"
LICENSE=""
SLOT="0"

KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist mirror strip"

MY_PN=${PN/-bin}

SRC_URI="
	amd64? ( https://github.com/deemru/Chromium-Gost/releases/download/${PV}/${MY_PN}-${PV}-linux-amd64.deb )
"

RDEPEND="
	!!www-client/chromium-gost
	app-accessibility/at-spi2-atk:2
	app-accessibility/at-spi2-core:2
	app-misc/ca-certificates
	dev-libs/atk
	dev-libs/expat
	dev-libs/glib:2
	dev-libs/nspr
	dev-libs/nss
	media-fonts/liberation-fonts
	media-libs/alsa-lib
	media-libs/mesa[gbm(+)]
	media-libs/vulkan-loader
	net-misc/curl[ssl]
	net-misc/wget
	net-print/cups
	sys-apps/dbus
	sys-libs/glibc
	virtual/libudev
	x11-libs/cairo
	x11-libs/gtk+
	x11-libs/libdrm
	x11-libs/libX11
	x11-libs/libXcomposite
	x11-libs/libXdamage
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXrandr
	x11-libs/libxcb[xkb(+)]
	x11-libs/libxkbcommon
	x11-libs/pango
	x11-misc/xdg-utils
	sys-apps/util-linux
	sys-apps/systemd-utils[udev(+)]
"

QA_PREBUILT="*"
QA_DESKTOP_FILE="usr/share/applications/*.desktop"
S=${WORKDIR}
BROWSER_HOME="opt/${MY_PN}"

pkg_setup() {
	chromium_suid_sandbox_check_kernel_config
}

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	rm -r etc || die

	rm -r "${BROWSER_HOME}/cron" || die

	gzip -d usr/share/doc/${MY_PN}*/changelog.gz || die
	gzip -d usr/share/man/man1/${MY_PN}-*.1.gz || die
	if [[ -L usr/share/man/man1/${MY_PN}.1.gz ]]; then
		rm usr/share/man/man1/${MY_PN}.1.gz || die
		dosym ${PN}-*.1 usr/share/man/man1/${PN}.1
	fi

	# Remove symbolic link. Ссылка имеет имя chromium-gost.1.gz
	if [[ -L usr/share/man/man1/${MY_PN}.1.gz ]]; then
	        rm usr/share/man/man1/${MY_PN}.1.gz || die
	fi

        mv usr/share/doc/${MY_PN}-* usr/share/doc/${PN}-${PVR} || die

	# Fix Gentoo bug https://bugs.gentoo.org/709450
	mv usr/share/appdata usr/share/metainfo

#	sed -i -E -e "s/^Icon=${MY_PN}$/Icon=${MY_PN}\.png/g" ${S}/${QA_DESKTOP_FILE}
	default
}

src_install() {
	mv * "${D}" || die

	keepdir "${EPREFIX}/${BROWSER_HOME}"
	for icon in "${D}/${BROWSER_HOME}/product_logo_"*.png; do
		size="${icon##*/product_logo_}"
		size=${size%.png}
		dodir "/usr/share/icons/hicolor/${size}x${size}/apps"
		newicon -s ${size} "$icon" ${MY_PN}.png
	done
}
