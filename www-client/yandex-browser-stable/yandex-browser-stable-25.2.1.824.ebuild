# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
#CHROMIUM_LANGS="cs de en-US es fr it ja kk pt-BR pt-PT ru tr uk uz zh-CN zh-TW"
inherit chromium-2 desktop unpacker pax-utils xdg

DESCRIPTION="The web browser from Yandex"
HOMEPAGE="https://browser.yandex.ru/"
LICENSE="Yandex-EULA"
SLOT="0"

KEYWORDS="~amd64"
IUSE="+ffmpeg-codecs"
RESTRICT="bindist mirror strip"

if [[ ${PN} == yandex-browser-beta ]] ; then
	suffix=beta
	MY_PN="yandex-browser-beta"

elif [[ ${PN} == yandex-browser-stable ]] ; then
	suffix=stable
	MY_PN="yandex-browser"
fi

CODECVER="0.95.0"

SRC_URI="
	amd64? ( https://repo.yandex.ru/yandex-browser/deb/pool/main/y/yandex-browser-${suffix}/yandex-browser-${suffix}_${PV}-1_amd64.deb -> ${P}.deb )
	ffmpeg-codecs? ( https://github.com/Ld-Hagen/nwjs-ffmpeg-prebuilt/releases/download/nwjs-ffmpeg-${CODECVER}/${CODECVER}-linux-x64.zip )
"

RDEPEND="
	!!www-client/yandex-browser-beta
	!!www-client/yandex-browser-corporate
	app-accessibility/at-spi2-atk:2
	app-accessibility/at-spi2-core:2
	app-misc/ca-certificates
	dev-libs/atk
	dev-libs/expat
	dev-libs/glib:2
	dev-libs/nspr
	dev-libs/nss
	media-fonts/liberation-fonts
	media-libs/alsa-lib
	media-libs/mesa[gbm(+)]
	net-misc/curl[ssl]
	net-misc/wget
	net-print/cups
	sys-apps/dbus
	sys-apps/util-linux
	sys-libs/glibc
	virtual/libudev
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:3[X]
	x11-libs/libdrm
	x11-libs/libX11
	x11-libs/libXcomposite
	x11-libs/libXdamage
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXrandr
	x11-libs/libxcb
	x11-libs/libxkbcommon
	x11-libs/libxkbfile
	x11-libs/pango
	x11-misc/xdg-utils
"

DEPEND="
	>=dev-util/patchelf-0.9
"

QA_PREBUILT="*"
QA_DESKTOP_FILE="usr/share/applications/.*\\.desktop"
S=${WORKDIR}
BROWSER_HOME="opt/${MY_PN/-//}"

pkg_setup() {
	chromium_suid_sandbox_check_kernel_config
}

src_prepare() {
	rm -r etc || die

	rm -r "${BROWSER_HOME}/cron" || die

	gzip -d usr/share/doc/${PN}/changelog.gz || die
	gzip -d usr/share/man/man1/${PN}.1.gz || die
	if [[ -L usr/share/man/man1/${PN}.1.gz ]]; then
		rm usr/share/man/man1/${PN}.1.gz || die
		dosym ${PN}.1 usr/share/man/man1/${PN}.1
	fi

	# Remove symbolic link
	if [[ -L usr/share/man/man1/${MY_PN}.1.gz ]]; then
	        rm usr/share/man/man1/${MY_PN}.1.gz || die
	fi

        mv usr/share/doc/${PN} usr/share/doc/${PN}-${PVR} || die

	# Fix Gentoo bug https://bugs.gentoo.org/709450
	mv usr/share/appdata usr/share/metainfo

	if use ffmpeg-codecs; then
		rm -f ${BROWSER_HOME}/libffmpeg.so || die

		mv libffmpeg.so ${BROWSER_HOME}/libffmpeg.so || die
#		ln -s /usr/lib64/chromium/libffmpeg.so.114 ${BROWSER_HOME}/libffmpeg.so && \
		elog "Ffmpeg codec update for yandex-browser completed in /${BROWSER_HOME}/libffmpeg.so"
	fi

	default

	patchelf --remove-rpath "${S}/${BROWSER_HOME}/yandex_browser-sandbox" || die "Failed to fix library rpath (yandex_browser-sandbox)"
	patchelf --remove-rpath "${S}/${BROWSER_HOME}/yandex_browser" || die "Failed to fix library rpath (yandex_browser)"
	patchelf --remove-rpath "${S}/${BROWSER_HOME}/find_ffmpeg" || die "Failed to fix library rpath (find_ffmpeg)"
}

src_install() {
	mv * "${D}" || die
	dodir "/usr/$(get_libdir)/${PN}/lib"

	# yandex_browser binary loads libudev.so at runtime
	dosym "${EPREFIX}/usr/$(get_libdir)/libudev.so" "${EPREFIX}/usr/$(get_libdir)/${PN}/lib/libudev.so.0"

	keepdir "${EPREFIX}/${BROWSER_HOME}"
	for icon in "${D}/${BROWSER_HOME}/product_logo_"*.png; do
		size="${icon##*/product_logo_}"
		size=${size%.png}
		dodir "/usr/share/icons/hicolor/${size}x${size}/apps"
		newicon -s ${size} "$icon" ${MY_PN}.png
	done

	fowners root:root "${EPREFIX}/${BROWSER_HOME}/yandex_browser-sandbox"
	fperms 4711 "${EPREFIX}/${BROWSER_HOME}/yandex_browser-sandbox"
	pax-mark m "${BROWSER_HOME}/yandex_browser-sandbox"
}

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
