# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..13} )
DISTUTILS_USE_PEP517=setuptools

inherit distutils-r1 gnome2-utils

DESCRIPTION="An onscreen keyboard useful for tablet PC users and for mobility impaired users"
HOMEPAGE="https://launchpad.net/onboard"
SRC_URI="https://github.com/martyr-deepin/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+ BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

COMMON_DEPEND="app-text/hunspell
	dev-libs/dbus-glib
	dev-python/dbus-python[${PYTHON_USEDEP}]
	dev-python/pycairo[${PYTHON_USEDEP}]
	dev-python/pygobject:3[${PYTHON_USEDEP}]
	gnome-base/dconf
	gnome-base/gsettings-desktop-schemas
	gnome-base/librsvg
	media-libs/libcanberra
	sys-apps/dbus
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:3[introspection]
	x11-libs/libX11
	x11-libs/libXi
	x11-libs/libXtst
	x11-libs/libwnck:3
	x11-libs/pango"

DEPEND="${COMMON_DEPEND}
	app-accessibility/at-spi2-core
	app-text/iso-codes
	dev-python/pyatspi[${PYTHON_USEDEP}]
	dev-util/intltool
	gnome-extra/yelp
	gnome-extra/mousetweaks
	sys-power/acpid
	x11-libs/libxkbfile
"

BDEPEND="${COMMON_DEPEND}
	dev-python/python-distutils-extra[${PYTHON_USEDEP}]
"
#	app-i18n/fcitx-gtk

RESTRICT="mirror"

src_prepare() {
	distutils-r1_src_prepare
	eapply_user

	echo "[Desktop Entry]
_Name=Onboard
_GenericName=Onboard onscreen keyboard
_Comment=Flexible onscreen keyboard
Exec=onboard
Terminal=false
Type=Application
Categories=Utility;Accessibility;
MimeType=application/x-onboard;
Icon=onboard
X-Ubuntu-Gettext-Domain=onboard
" > data/onboard.desktop.in

	echo "[Desktop Entry]
_Name=Onboard
_GenericName=Onboard onscreen keyboard
_Comment=Flexible onscreen keyboard
Exec=onboard --not-show-in=GNOME,GNOME-Classic:GNOME --startup-delay=3.0
Icon=onboard
Type=Application
NoDisplay=true
X-Ubuntu-Gettext-Domain=onboard
AutostartCondition=GSettings org.gnome.desktop.a11y.applications screen-keyboard-enabled
X-GNOME-AutoRestart=true
OnlyShowIn=GNOME;Unity;MATE;
" > data/onboard-autostart.desktop.in

	cp -f "${FILESDIR}/"*.po "${S}/po/"
}

src_install() {
	distutils-r1_src_install
}

pkg_preinst() {
	gnome2_icon_savelist
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
	gnome2_schemas_update
	xdg_desktop_database_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	gnome2_schemas_update
	xdg_desktop_database_update
}
