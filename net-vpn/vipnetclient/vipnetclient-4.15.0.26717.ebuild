# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit desktop unpacker xdg

DESCRIPTION="ViPNet Client for Linux GUI by JSC InfoTeCS. CLI version"
HOMEPAGE="https://www.vipnet.ru"

MY_PV=${PV/0./0-}
MY_PN=${PN/-gost/_gost}

SRC_URI="
	amd64? ( https://git.calculate-linux.org/serg-sg/sources/raw/branch/master/net-vpn/vipnetclient/${MY_PN}_ru_amd64_${MY_PV}.deb )
"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="bindist mirror strip"

RDEPEND="
	!net-vpn/vipnetclient-gui
	!net-vpn/vipnetclient-gui-gost
	!net-vpn/vipnetclient-gost
	sys-process/procps
"

S=${WORKDIR}

pkg_setup() {

	# Sandbox Disabled

	UPGRADE_DIR="/tmp/vipnetclient"
	rm -rf ${UPGRADE_DIR}
	mkdir -p ${UPGRADE_DIR}

	# Creating file-markers on each existing vipnetclient process and send them USR1
	for pid in $(pgrep -x /usr/bin/vipnetclient); do
		local uid=$(stat -c "%u" /proc/${pid} 2>/dev/null)
		[ -z "$uid" ] && continue
		[ $uid -ne 0 ] && continue						# select only root owned processes
		[ $(stat -c "%u" /proc/${pid} 2>/dev/null) -eq 0 ] || continue		# select only root owned processes
		local user=$(grep -awz USER /proc/${pid}/environ 2>/dev/null | tr -d '\0' | cut -c 6-)
		[ -z "$user" ] && continue
		echo ${pid} >${UPGRADE_DIR}/${user}
	done

	local sig=USR1
	local proc_name=vipnetclient

	if which pkill >/dev/null 2>&1; then
		pkill -${sig} -x ${proc_name}
	elif which killall >/dev/null 2>&1; then
		killall -${sig} ${proc_name}
	fi

	[ -x /usr/bin/vipnetclient-webui ] && /etc/init.d/vipnetclient-webui restart
}

src_unpack() {

	unpack_deb ${A}

}

src_install() {

	mv * "${D}" || die

	# Set owners
	chown root:root ${D}/usr/bin/vipnetclient 2>/dev/null
	chown root:root $(ls -1 ${D}/etc/init.d/*vipnetclient) 2>/dev/null
	chmod 755 $(ls -1 ${D}/etc/init.d/*vipnetclient) 2>/dev/null

}

pkg_postinst() {

	# Sandbox Disabled

	# Set capability
	/usr/bin/vipnetclient --setcap --version

	# update man db
	mandb >/dev/null 2>&1

	# create service and init scripts
	if which systemctl >/dev/null 2>&1; then
		if [ -f /etc/systemd/system/vipnetclientd.service ]; then
			systemctl enable vipnetclientd >/dev/null 2>&1 && elog "Служба vipnetclientd установлена"
		else
			systemctl enable vipnetclient >/dev/null 2>&1 && elog "Служба vipnetclient установлена"
		fi
		[ -x /usr/bin/vipnetclient-webui ] && systemctl enable vipnetclient-webui >/dev/null 2>&1 && elog "Служба vipnetclient-webui установлена"

		systemctl reset-failed
		systemctl stop vipnetclientdaemon*

	elif which update-rc.d >/dev/null 2>&1; then
		update-rc.d vipnetclient defaults 90 10 >/dev/null 2>&1
		[ -x /usr/bin/vipnetclient-webui ] && update-rc.d vipnetclient-webui defaults 90 10 >/dev/null 2>&1

	elif which rc-update >/dev/null 2>&1; then
		rc-update add vipnetclient default >/dev/null 2>&1 && elog "Служба vipnetclient установлена"
		[ -x /usr/bin/vipnetclient-webui ] && rc-update add vipnetclient-webui default >/dev/null 2>&1 && elog "Служба vipnetclient-webui установлена"
	fi

	[ -x /usr/bin/vipnetclient-webui ] && /etc/init.d/vipnetclient-webui start

	# Update shared libraries paths
	which ldconfig >/dev/null 2>&1 && ldconfig 2>/dev/null

	# for some distrs
	find /var/lib/vipnet -type d -exec chmod 755 {} \;

}

pkg_prerm ()  {

	# Sandbox Disabled

	# Размонтируем виртуальный диск
	umount /home/*/.vipnet/var/run
	umount /root/.vipnet/var/run

	#update man db
	mandb >/dev/null 2>&1

	# Update shared libraries paths
	which ldconfig >/dev/null 2>&1 && ldconfig 2>/dev/null

	# disable service and remove init scripts
	if which systemctl >/dev/null 2>&1; then
		systemctl disable vipnetclient >/dev/null 2>&1
		systemctl disable vipnetclientd >/dev/null 2>&1
		[ -x /usr/bin/vipnetclient-webui ] && systemctl disable vipnetclient-webui >/dev/null 2>&1
	elif which update-rc.d >/dev/null 2>&1; then
		update-rc.d -f vipnetclient remove >/dev/null 2>&1
		[ -x /usr/bin/vipnetclient-webui ] && update-rc.d -f vipnetclient-webui remove >/dev/null 2>&1
	elif which rc-update >/dev/null 2>&1; then
		rc-update del vipnetclient >/dev/null 2>&1
		[ -x /usr/bin/vipnetclient-webui ] && rc-update del vipnetclient-webui >/dev/null 2>&1
	fi

	# При новой установке REPLACING_VERSIONS = "", при новой установке pkg_postrm() не выполняется.
	# При удалении REPLACING_VERSIONS не вычисляется, этап pkg_postinst() не выполняется.
	# При удалении REPLACED_BY_VERSION = "", при новой установке pkg_postrm() не выполняется.
	# При обновлении или переустановки выполняются этапы pkg_postinst() и pkg_postrm()
	# и, соответственно, переменные REPLACING_VERSIONS и REPLACED_BY_VERSION имеют ненулевое значение.
	# Таким образом мы можем определить, что пакет устанавливается, обновляется, переустанавливается поверх или удаляется
	# и выстраивать соответствующую логику.

	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		# Удалим мусор при окончательном удалении
		elog "Пакет ${PN} окончательно удаляется"
		rm -rf /etc/systemd/system/vipnetclient*
		rm -f /etc/rc*/*vipnetclient* >/dev/null 2>&1
		rm -f /etc/vipnet_token.conf
	fi

}
