# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit desktop unpacker xdg

DESCRIPTION="Legacy Launcher is a simple, lightweight and yet highly customizeable alternative Minecraft launcher.~"
HOMEPAGE="https://llaun.ch/"
LICENSE="GPL3-0"
SLOT="0"

KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist strip mirror"
MY_PV=$(ver_rs 1- '')
MY_PN="TL_legacy"

SRC_URI="
	https://llaun.ch/repo/downloads/${MY_PN}.deb -> ${P}.deb
"

RDEPEND="
	app-shells/bash
	>=dev-java/openjdk-jre-bin-8
	net-misc/wget
"

QA_PREBUILT="*"
S=${WORKDIR}

SHARED_BOOTSTRAP_DIR="/usr/lib/legacylauncher"
SHARED_BOOTSTRAP="${SHARED_BOOTSTRAP_DIR}/bootstrap.jar"

src_unpack() {
	unpack_deb ${P}.deb
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	mkdir -p "$SHARED_BOOTSTRAP_DIR" || die

	for host in llaun.ch eu1.llaun.ch lln4.ru ru1.lln4.ru
	do
	    elog "Trying $host..."
	    wget https://${host}/jar -O "${SHARED_BOOTSTRAP}" && break
	done

	ls -la "${SHARED_BOOTSTRAP_DIR}"

	if [ ! -f "${SHARED_BOOTSTRAP}" ]
	then
	    elog "Failed to download bootstrap" >&2
	    exit 1
	fi

	xdg_desktop_database_update
}

pkg_postrm() {
	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		# Удалим мусор при окончательном удалении
		elog "Пакет ${PN} окончательно удаляется"
		rm -rf "${SHARED_BOOTSTRAP_DIR}"
	fi

	xdg_desktop_database_update
}
