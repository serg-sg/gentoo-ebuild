# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10,11,12,13} )

inherit flag-o-matic linux-info python-single-r1 systemd tmpfiles unpacker

KEYWORDS="~amd64"

SLOT=$(ver_cut 1 )

MY_PN=${PN/_1c/-1c}
MY_PV=$(ver_rs 2 '-')
MY_VR=$(ver_cut 1-2)
#S="${WORKDIR}/${MY_PN}-${MY_PV}"
S=${WORKDIR}

RESTRICT="bindist strip mirror"

SRC_URI="
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}_${MY_PV}.bookworm_amd64.deb -> ${P}.deb

	client? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-client_${MY_PV}.bookworm_amd64.deb -> ${P}-client.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-client-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-client-dbgsym.deb
	)
)
	server? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-server_${MY_PV}.bookworm_amd64.deb -> ${P}-server.deb
http://ftp.ru.debian.org/debian/pool/main/i/icu/libicu72_72.1-5%2Bb1_amd64.deb -> libicu72_amd64-${PVR}.deb
http://ftp.ru.debian.org/debian/pool/main/s/systemd/libsystemd0_257.1-4_amd64.deb -> libsystemd0_amd64-${PVR}.deb
http://deb.debian.org/debian/pool/main/o/openldap/libldap-2.5-0_2.5.19+dfsg-1_amd64.deb -> libldap-2.5_amd64-${PVR}.deb
http://ftp.ru.debian.org/debian/pool/main/c/cyrus-sasl2/libsasl2-2_2.1.28%2Bdfsg-10_amd64.deb -> libsasl2-2-2.1.28_amd64-${PVR}.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-server-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-server-dbgsym.deb
	)
)
	doc? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-docs_${MY_PV}.bookworm_all.deb -> ${P}-docs.deb
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-docs-ru_${MY_PV}.bookworm_all.deb -> ${P}-docs-ru.deb
 )
	dev? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-dev_${MY_PV}.bookworm_amd64.deb -> ${P}-dev.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-dev-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-dev-dbgsym.deb
	)
)
	python? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-plpython3_${MY_PV}.bookworm_amd64.deb -> ${P}-plpython3.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-plpython3-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-plpython3_dbgsym.deb
	)
)
	perl? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-plperl_${MY_PV}.bookworm_amd64.deb -> ${P}-plperl.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-plperl-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-plperl-dbgsym.deb
	)
)
	tcl? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-pltcl_${MY_PV}.bookworm_amd64.deb -> ${P}-pltcl.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-pltcl-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-pltcl-dbgsym.deb
	)
)
	jit? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-jit_${MY_PV}.bookworm_amd64.deb -> ${P}-jit.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-jit-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-jit-dbgsym.deb
	)
)
	libs? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-libs_${MY_PV}.bookworm_amd64.deb -> ${P}-libs.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-libs-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-libs-dbgsym.deb
	)
)
	contrib? (
https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-contrib_${MY_PV}.bookworm_amd64.deb -> ${P}-contrib.deb
		debug? (
	https://repo.postgrespro.ru/1c/archive/1c-${MY_VR}/debian/pool/main/p/${MY_PN}-${SLOT}/${MY_PN}-${SLOT}-contrib-dbgsym_${MY_PV}.bookworm_amd64.deb -> ${P}-contrib-dbgsym.deb
	)
)
"

LICENSE="POSTGRESQL GPL-2"
DESCRIPTION="Postgres Pro 1c — это объектно-реляционная система управления базами данных SQL."
HOMEPAGE="http://www.postgrespro.ru/"

IUSE="+client server doc dev python perl tcl jit libs contrib debug systemd"

REQUIRED_USE="
client?		( server )
server?		( client contrib )
dev?		( libs )
python?		( server contrib ${PYTHON_REQUIRED_USE} )
perl?		( server contrib )
tcl?		( server )
jit?		( server )
contrib?	( server libs )
"

CDEPEND="
>=app-eselect/eselect-postgresql-2.0
acct-group/postgres
acct-user/postgres

client? (
	sys-libs/glibc
	app-arch/lz4
	sys-libs/readline
	dev-libs/openssl
	app-arch/zstd
	sys-libs/zlib
)
server? (
	sys-libs/glibc
	app-crypt/mit-krb5
	net-nds/openldap
	dev-libs/cyrus-sasl
	app-arch/lz4
	sys-libs/pam
	dev-libs/openssl
	sys-libs/libunwind
	dev-libs/libxml2
	app-arch/zstd
	sys-libs/zlib
	sys-apps/coreutils
	sys-apps/grep
	sys-apps/sed
	sys-libs/timezone-data
	dev-libs/openssl
	sys-apps/locale-gen
	net-misc/oidentd
	systemd? ( sys-apps/systemd )
 )
dev? (
	sys-libs/glibc
	dev-lang/perl
	dev-perl/IPC-Run
	app-arch/dpkg
	dev-libs/libxslt
	dev-libs/libxml2
	dev-libs/icu
	dev-libs/openssl
	sys-libs/zlib
	sys-libs/readline
	app-arch/zstd
	app-arch/lz4
	app-crypt/mit-krb5
	sys-libs/pam
)
python? (
	sys-libs/glibc
	${PYTHON_DEPS}
)
perl? (
	sys-libs/glibc
	>=dev-lang/perl-5.8:=
)
tcl? (
	sys-libs/glibc
	>=dev-lang/tcl-8:0=
)
jit? (
	sys-libs/glibc
	sys-devel/gcc
)
libs? (
	sys-libs/glibc
	app-crypt/mit-krb5
	net-nds/openldap
	dev-libs/openssl
)
contrib? (
	sys-libs/glibc
	dev-libs/openssl
	sys-apps/util-linux
	dev-libs/libxml2
	dev-libs/libxslt
	sys-libs/zlib
	perl? (
		dev-db/libdbi-drivers[postgres(+)]
	)
)
"

RDEPEND="${CDEPEND}"
OPTDIR="opt/pgpro/1c-${SLOT}"
DEFCONFIG="/etc/default/postgrespro-1c-${SLOT}"
PGDATADIR="/var/lib/pgpro/1c-${SLOT}/data"
BINDIR="/opt/pgpro/1c-${SLOT}/bin"

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_unpack() {
	unpack_deb ${P}.deb || die

	if use server ; then
		unpack_deb ${P}-server.deb || die
		unpack_deb libicu72_amd64-${PVR}.deb || die
		unpack_deb libldap-2.5_amd64-${PVR}.deb || die
		unpack_deb libsystemd0_amd64-${PVR}.deb || die
		unpack_deb libsasl2-2-2.1.28_amd64-${PVR}.deb || die
		if use debug ; then
			unpack_deb ${P}-server-dbgsym.deb || die
		fi
	fi

	if use client ; then
		unpack_deb ${P}-client.deb || die
		if use debug ; then
			unpack_deb ${P}-client-dbgsym.deb || die
		fi
	fi

	if use doc ; then
		unpack_deb ${P}-docs.deb || die
		unpack_deb ${P}-docs-ru.deb || die
	fi

	if use dev ; then
		unpack_deb ${P}-dev.deb || die
		if use debug ; then
			unpack_deb ${P}-dev-dbgsym.deb || die
		fi
	fi

	if use python ; then
		unpack_deb ${P}-plpython3.deb || die
		if use debug ; then
			unpack_deb ${P}-plpython3_dbgsym.deb || die
		fi
	fi

	if use perl ; then
		unpack_deb ${P}-plperl.deb || die
		if use debug ; then
			unpack_deb ${P}-plperl-dbgsym.deb || die
		fi
	fi

	if use tcl ; then
		unpack_deb ${P}-pltcl.deb || die
		if use debug ; then
			unpack_deb ${P}-pltcl-dbgsym.deb || die
		fi
	fi

	if use jit ; then
		unpack_deb ${P}-jit.deb || die
		if use debug ; then
			unpack_deb ${P}-jit-dbgsym.deb || die
		fi
	fi

	if use libs ; then
		unpack_deb ${P}-libs.deb || die
		if use debug ; then
			unpack_deb ${P}-libs-dbgsym.deb || die
		fi
	fi

	if use contrib ; then
		unpack_deb ${P}-contrib.deb || die
		if use debug ; then
			unpack_deb ${P}-contrib-dbgsym.deb || die
		fi
	fi
}

src_prepare() {
	# Перемещаем файлы библиотек в нужное место
	if use server ; then
		mkdir lib64
		mv usr/lib/x86_64-linux-gnu/lib* "lib64" || die

		# Удалить лишнее от распаковки библиотек
		rm -r usr/share/doc/libicu72 || die
		rm -r usr/share/doc/libldap* || die
		rm -r usr/share/doc/libsystemd0 || die
		rm -r usr/share/doc/libsasl2-2 || die
		rm -r usr/share/lintian || die
		rm -r usr/share/man || die
		rm -r usr/lib/x86_64-linux-gnu || die
		rm -r usr/lib/sasl2 || die

		if which rc-update    >/dev/null 2>&1; then
			sed -i -E 's/^. \/lib\/lsb\/init-functions/#. \/lib\/lsb\/init-functions/' etc/init.d/postgrespro-1c-17
			sed -i 's/log_success_msg OK/echo -n " - OK"/' etc/init.d/postgrespro-1c-17
			sed -i 's/log_failure_msg FAILED/echo -n " - FAILED"/' etc/init.d/postgrespro-1c-17
			sed -i -E 's/log_success_msg STOPPED/echo -n " - STOPPED"/' etc/init.d/postgrespro-1c-17
		fi
	fi

	default
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	postgresql-config update

	if use server ; then

		set -e
		# Подготовка home
		[ -d ~postgres ] || mkdir -p ~postgres
		chown postgres:postgres ~postgres
		chmod 700 ~postgres
		if [ ! -d /var/lib/pgpro/1c-17 ]; then
			mkdir -p /var/lib/pgpro/1c-17
			chown postgres:postgres /var/lib/pgpro/1c-17
			chmod 755 /var/lib/pgpro/1c-17
		else
		# Create /etc/default/postgrespro-1c-17
		# If we are upgrading from previous minor release where this file
		# didn't exist and we have database under default PATH
		# Создадим /etc/default/postgrespro-1c-17
		# Если мы обновляемся с предыдущей младшей версии, где этот файл
		# не существовал, и у нас есть база данных по умолчанию PATH

			if [ -f "$PGDATADIR/PG_VERSION" -a ! -f "$DEFCONFIG" ]; then
				echo "PGDATA=$PGDATADIR" > "$DEFCONFIG"
			fi
		fi

		if [ -f /opt/pgpro/1c-${SLOT}/bin/pg_integrity_check ]; then
			echo "Recalculating checksums - stage $1"
			/opt/pgpro/1c-${SLOT}/bin/pg_integrity_check -o -s || true
		fi

		if [ -f /etc/default/postgrespro-1c-${SLOT} ]; then
			if which systemctl >/dev/null 2>&1; then
				systemctl daemon-reload >/dev/null 2>&1
			fi

			. /etc/default/postgrespro-1c-17
			if [ ! "$ENABLE_ONLINE_UPGRADE" = "1" ]; then
				if which systemctl >/dev/null 2>&1; then
					/opt/pgpro/1c-${SLOT}/bin/pg-setup service condrestart
				elif which rc-update    >/dev/null 2>&1; then
					rc-service postgrespro-1c-${SLOT} reload
				fi
			fi
		fi

		elog "To set up the initial database environment, run the command:"
		elog "Чтобы настроить начальную среду базы данных, выполните команду:"
		elog "    emerge --config =${CATEGORY}/${PF}"

	fi

	if use jit ; then
		elog
		elog "После установки этого пакета вам необходимо добавить переменную jit=on в"
		elog "postgesql.conf вашей базы данных, чтобы включить jit."
		elog
	fi
}

pkg_prerm() {
	if use server && [[ -z ${REPLACED_BY_VERSION} ]] ; then
		ewarn "Have you dumped and/or migrated the ${SLOT} database cluster?"
		ewarn "\thttps://wiki.gentoo.org/wiki/PostgreSQL/QuickStart#Migrating_PostgreSQL"
		ebegin "Resuming removal in 10 seconds (Control-C to cancel)"

		ewarn "Вы сбросили и/или перенесли кластер базы данных ${SLOT}?'"
		ewarn "\thttps://wiki.gentoo.org/wiki/PostgreSQL/QuickStart#Migrating_PostgreSQL"
		ebegin "Возобновление удаления через 10 секунд (Control-C для отмены)"

		sleep 10

		if [ -f /etc/default/postgrespro-1c-17 ] ; then
			if which systemctl >/dev/null 2>&1; then
				/opt/pgpro/1c-${SLOT}/bin/pg-setup service stop || true
				/opt/pgpro/1c-${SLOT}/bin/pg-setup service disable || true
				elog "Служба ${PN} остановлена и удалена"
			elif which rc-update    >/dev/null 2>&1; then
				rc-service postgrespro-1c-${SLOT} stop \
				&& rc-update del postgrespro-1c-${SLOT} default \
				&& elog "Служба ${PN} остановлена и удалена"
			fi
		fi

		/opt/pgpro/1c-${SLOT}/bin/pg-wrapper links remove || true
		eend 0
	fi
}

pkg_postrm() {
	postgresql-config update
}

pkg_config() {
	use server || die "USE flag 'server' not enabled. Nothing to configure.\nUSE-флаг 'server' не включен. Нечего настраивать."

	if use server ; then
		# Following actions are carried out only if we are installing
		# first time. If we are upgrading (second arg is not empty) skip
		# them all.
		# Следующие действия выполняются только если мы устанавливаем в первый раз.
		# Если мы обновляем (второй аргумент не пустой) пропустить их все.

		$BINDIR/pg-wrapper links update

			if [ ! -f $PGDATADIR/PG_VERSION ]; then
				free_port=$($BINDIR/pg-setup find-free-port)
				$BINDIR/pg-setup initdb
				if [ "$free_port" != 5432 ]; then
					elog "Set 1c-17 PGPORT to $free_port"
					$BINDIR/pg-setup set-server-port "$free_port"
				fi
			fi

		if which systemctl >/dev/null 2>&1; then
			$BINDIR/pg-setup service enable
			$BINDIR/pg-setup service start
			systemctl enable postgrespro-1c-${SLOT}.service
			elog "Служба ${PN} установлена"
		elif which rc-update	>/dev/null 2>&1; then
			rc-update add postgrespro-1c-${SLOT} default \
			&& rc-service postgrespro-1c-${SLOT} start \
			&& elog "Служба ${PN} установлена и запущена"
		fi
	fi
}
