# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg unpacker

DESCRIPTION="MyOffice is a set of office applications for creating text, table and presentation documents. \
MyOffice is a full stack of office applications which are integrated into a single platform and intended to exchange e-mails, \
maintain a list of contacts, manage joint meetings in the calendar, as well as collaboratively create and edit documents \
(tables, text documents, presentations)."

HOMEPAGE="https://myoffice.ru/products/standard/"
LICENSE="MyOffice"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist mirror strip"

if [[ ${PN} == myoffice-standard-home-edition ]] ; then
	MY_PN="myoffice-standard-home-edition"

elif [[ ${PN} == myoffice-standard-documents ]] ; then
	MY_PN="myoffice-standard"
fi

SRC_URI="MyOffice_Standard_Documents_2.3-3_amd64.deb"

RDEPEND="
	>=sys-libs/glibc-2.17
"

S=${WORKDIR}

pkg_nofetch() {
	elog "Необходимо получить от разработчика или поставщика установочный файл ${A},"
	elog "который необходимо поместить в директорию DISTDIR,"
	elog "расположенный по адресу ${DISTDIR}."
	elog "После этого повторите установку."
	elog "Для копирования установочного файла в директорию DISTDIR потребуются права root."
}

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	# Перемещаем mime
	path=usr/share/mime/application
	mkdir -p ${path}
	for xml in "${S}/usr/local/bin/${MY_PN}/"*.xml; do
		file="${xml##*/myoffice_}"
		file=${myoffice_documents_file%.xml}
		mv ${xml} ${path}/${file} || die
	done

	# Перемещаем ярлыки приложений
	path=usr/share/applications
	mkdir -p ${path}
	mv "${S}/usr/local/bin/${MY_PN}/"*.desktop ${path} || die

	# Исправляем строки запуска в ярлыках запуска
	sed -i -E -e "s/ %u$/.sh %u\nPath=\/opt\/${MY_PN}/" ${path}/myoffice*.desktop
	sed -i -E -e "s/^Exec=myoffice/Exec=\/opt\/${MY_PN}\/myoffice/" ${path}/myoffice*.desktop
	echo Path=/opt/${MY_PN} >> ${path}/myoffice*.desktop

	# Перемещаем иконки приложений в формате svg
	path=usr/share/icons/hicolor/scalable/apps
	mkdir -p ${path}
	mv "${S}/usr/local/bin/${MY_PN}/icons/app_text_icon.svg" ${path}/myoffice-text.svg || die
	mv "${S}/usr/local/bin/${MY_PN}/icons/app_sheet_icon.svg" ${path}/myoffice-spreadsheet.svg || die
	mv "${S}/usr/local/bin/${MY_PN}/icons/app_point_icon.svg" ${path}/myoffice-presentation.svg || die

	APPS="text sheet point"
	for app in ${APPS}; do
		for icon in "${S}/usr/local/bin/${MY_PN}/icons/app_${app}_icon_"*.png; do
			size="${icon##*/app_${app}_icon_}"
			size=${size%.png}
			mkdir -p "usr/share/icons/hicolor/${size}x${size}/apps"
			if [ ${app} = "text" ]; then
				cp ${icon} "usr/share/icons/hicolor/${size}x${size}/apps/myoffice-${app}.png" || die
			elif [ ${app} = "sheet" ]; then
				cp ${icon} "usr/share/icons/hicolor/${size}x${size}/apps/myoffice-spread${app}.png" || die
			elif [ ${app} = "point" ]; then
				cp ${icon} "usr/share/icons/hicolor/${size}x${size}/apps/myoffice-${app}.png" || die
			fi
		done
	done

	# Перемещаем в opt
	mkdir -p "${S}/opt"
	mv "${S}/usr/local/bin/${MY_PN}" "${S}/opt/" || die

	default
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
	vxdg_icon_cache_update

}

pkg_postrm() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
	xdg_icon_cache_update
}
