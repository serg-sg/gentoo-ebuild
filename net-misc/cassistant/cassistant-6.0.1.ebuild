# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit unpacker

DESCRIPTION="Ассистент - удобный инструмент для безопасного удалённого доступа. Консольная версия."
HOMEPAGE="https://мойассистент.рф/"
SRC_URI="
https://xn--80akicokc0aablc.xn--p1ai/%D1%81%D0%BA%D0%B0%D1%87%D0%B0%D1%82%D1%8C/Download/1092 -> ${P}.deb
"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"

# Зависимости для запуска этой программы
RDEPEND="
	!net-misc/assistant
	app-shells/bash
"

RESTRICT="bindist mirror strip"

S=${WORKDIR}

MY_PN=${PN/c}

ASISTDIR="opt/${MY_PN}"
SCRIPTS_DIR="/${ASISTDIR}/scripts"

pkg_setup() {

	# Sandbox Disabled

	CRON_REGEXP_PATTERN="^\*\/1 \* \* \* \* root \/opt\/assistant\/scripts\/ast_restart\.sh$"
	CRON_FILE="/etc/crontab"
	sed -i "/${CRON_REGEXP_PATTERN}/d" ${CRON_FILE}

	# Завершаем процессы, иначе не дают обновить или преустановить ранее установленную версию
	killall assistant 2> /dev/null
	killall asts 2> /dev/null

}

src_install() {

	mv * "${D}" || die
	sed -i -E -e '/^\s+eselect rc stop zassistantd/d' "${D}${SCRIPTS_DIR}/daemon.sh"
	sed -i -E -e 's/exec \/opt\/assistant\/bin\/asts/exec \/opt\/assistant\/bin\/asts --console/g' \
		"${D}${SCRIPTS_DIR}/openrc.t"

}

pkg_preinst() {

	# Sandbox	Disabled

	# Завершаем процессы, иначе не дают обновить или преустановить ранее установленную версию
	killall assistant 2> /dev/null
	killall asts 2> /dev/null

}

pkg_postinst() {

	# Sandbox	Disabled

	# Исправляем права на файлы и папки
	chmod -R +x /${ASISTDIR}/{bin}
	chmod -R +x ${SCRIPTS_DIR}
	mkdir -p /${ASISTDIR}/{chat,log,screenshot,video}
	chmod -R a+rw /${ASISTDIR}/{chat,license,log,screenshot,video}
	rm -rf /root/.config/cassistant/
	mkdir -p /root/.config/cassistant
	ln -s /.config/cassistant/assistant.ini /root/.config/cassistant/assistant.ini
	chmod 666 /root/.config/cassistant/assistant.ini

	${SCRIPTS_DIR}/setup.sh --install

	elog "--------------------------------------------------------------------------"
	elog "Для получени ID и пароля подключения запустите"
	elog "\"/opt/assistant/bin/asts --console\"."
	elog "После того, как будет установлено соединение с сервером,"
	elog "нажмите на клавиатуре клавишу [i]."
	elog
	elog "Для установки постоянного пароля убедитесь, что процессы asts завершены."
	elog "Процессы можно завершить командой \"sudo killall asts 2> /dev/null\"."
	elog
	elog "Далее запустите \"sudo /opt/assistant/bin/asts --console -p <пароль>\"."
	elog "--------------------------------------------------------------------------"

}

pkg_prerm() {

	killall assistant 2> /dev/null
	killall asts 2> /dev/null

	# При новой установке REPLACING_VERSIONS = "", при новой установке pkg_postrm() не выполняется.
	# При удалении REPLACING_VERSIONS не вычисляется, этап pkg_postinst() не выполняется.
	# При удалении REPLACED_BY_VERSION = "", при новой установке pkg_postrm() не выполняется.
	# При обновлении или переустановки выполняются этапы pkg_postinst() и pkg_postrm()
	# и, соответственно, переменные REPLACING_VERSIONS и REPLACED_BY_VERSION имеют ненулевое значение.
	# Таким образом мы можем определить, что пакет устанавливается, обновляется, переустанавливается поверх или удаляется
	# и выстраивать соответствующую логику.

	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		# Удалить ярлык
		rm -f /usr/share/applications/assistant.desktop || die

		if which rc-update >/dev/null 2>&1; then
			# Зачистка мусора
			rc-update delete zassistantd
			/etc/init.d/zassistantd stop

			# Удалить службу
			rm -f /etc/init.d/zassistantd || die
		fi

		${SCRIPTS_DIR}/setup.sh --uninstall

		# Удалить каталог с журналами после удаления
		rm -r -f /opt/assistant || die
		rm -r -f /.config/${PN} || die
	fi

}

pkg_postrm() {

	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		elog "Пакет ${P} окончательно удаляется"

		# Удалить каталог с журналами после удаления
		rm -r -f /opt/assistant || die
		rm -r -f /.config/${PN} || die
	fi

}
