# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="CUPS and SANE drivers for Pantum series printer and scanner."
HOMEPAGE="https://www.pantum.ru/support/download/driver/"

inherit udev

IUSE="scanner"

MY_PV=${PV//./_}

SRC_URI="https://drivers.pantum.ru/userfiles/files/download/drive/2013/0619/Pantum%20Ubuntu%20Driver%20V${MY_PV}(1).zip"

LICENSE="AS-IS"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="bindist mirror strip"

COMMON_DEPEND="
	net-print/cups
	net-print/cups-filters
	sys-apps/dbus
	>=sys-libs/glibc-2.0.0
	media-libs/libjpeg8
	scanner? (
		media-gfx/sane-backends
	)
"

BDEPEND="
	virtual/pkgconfig
	app-arch/unzip
"
#DEPEND="
#	${COMMON_DEPEND}
#"
RDEPEND="
	${COMMON_DEPEND}
	app-text/ghostscript-gpl
"

S="${WORKDIR}/Pantum Ubuntu Driver V${PV}"

src_prepare() {
	eapply_user
	unpack "${S}/Resources/pantum_${PV}-1_amd64.deb" || die
#	tar -xvf "${S}/data.tar.xz" -C "$D" || die
	tar -xvf "${S}/data.tar.xz" || die
}

src_install() {

	echo "D = $D"

	if ! use scanner ; then
		rm -rf "${S}/usr/local" || die
	else
		insinto /etc/sane.d/
		doins -r etc/sane.d/*
		udev_dorules etc/udev/rules.d/*.rules
		into /usr/$(get_libdir)/
		insinto /usr/$(get_libdir)/
		doins -r usr/lib/x86_64-linux-gnu/sane

	fi

	insinto /opt/pantum
	doins -r opt/pantum/*

	insinto /usr/local
	doins -r usr/local/*

	exeinto /usr/libexec/cups/filter
	doexe usr/lib/cups/filter/*
	insinto /usr/share/cups/model
	doins -r usr/share/cups/model/Pantum

	dodoc -r usr/share/doc/${PN}/*

	mkdir -p "${D}/etc/ld.so.conf.d/" || die
	echo "/opt/pantum/lib" >> "${D}/etc/ld.so.conf.d/pantum.conf" || die
}

pkg_postinst() {
	mkdir -p /opt/pantum/lib || die
	ldconfig
	udev_reload
}

pkg_prerm() {
	rm -rf /etc/ld.so.conf.d/pantum.conf
	ldconfig
	udev_reload
}
