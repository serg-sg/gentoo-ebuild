# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit unpacker xdg

DESCRIPTION="Kontur.Talk"
HOMEPAGE="https://ktalk.ru"
LICENSE="ISC"
SLOT="0"

KEYWORDS="~amd64 ~arm64"
RESTRICT="bindist strip mirror"

SRC_URI="
	amd64? ( https://st.${PN}.host/${PN}-app/linux/${PN}${PV}amd64.deb )
	arm64? ( https://st.${PN}.host/${PN}-app/linux/${PN}${PV}arm64.deb )
"

RDEPEND="
	x11-libs/gtk+:3
	dev-libs/nss
	x11-libs/libXtst
	x11-misc/xdg-utils
	app-accessibility/at-spi2-core
	sys-apps/util-linux
	dev-libs/libappindicator
"

S=${WORKDIR}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	# Sandbox	Disabled

	if type update-alternatives 2>/dev/null >&1; then
	    # Remove previous link if it doesn't use update-alternatives
	    if [ -L '/usr/bin/ktalk' -a -e '/usr/bin/ktalk' -a "`readlink '/usr/bin/ktalk'`" \
			!= '/etc/alternatives/ktalk' ]; then
		rm -f '/usr/bin/ktalk'
	    fi
	    update-alternatives --install '/usr/bin/ktalk' 'ktalk' '/opt/Толк/ktalk' 100
	else
	    ln -sf '/opt/Толк/ktalk' '/usr/bin/ktalk'
	fi

	# SUID chrome-sandbox for Electron 5+
	chmod 4755 '/opt/Толк/chrome-sandbox' || true

#	update-mime-database /usr/share/mime || true
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		elog "Пакет ${P} окончательно удаляется"

		# Delete the link to the binary
		if type update-alternatives >/dev/null 2>&1; then
		    update-alternatives --remove 'ktalk' '/usr/bin/ktalk'
		else
		    rm -f '/usr/bin/ktalk'
		fi

	fi

	xdg_desktop_database_update
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}
