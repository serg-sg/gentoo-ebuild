# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
CHROMIUM_LANGS="en-US ru"
inherit chromium-2 desktop unpacker pax-utils xdg

DESCRIPTION="Веб браузер Спутник"
HOMEPAGE="https://sputnik-lab.com/"
LICENSE=""
SLOT="0"

KEYWORDS="amd64"
#IUSE=""
RESTRICT="bindist mirror strip"

if [[ ${PN} == sputnik-browser-beta ]] ; then
	suffix=beta
	MY_PN="sputnik-browser-beta"

elif [[ ${PN} == sputnik-browser-stable ]] ; then
	suffix=stable
	MY_PN="sputnik-browser"
fi

SRC_URI="
	amd64? ( https://git.calculate-linux.org/serg-sg/sources/raw/branch/master/www-client/sputnik-browser-stable/sputnik-browser-${suffix}_${PV}-1_amd64.deb -> ${P}.deb )
"

RDEPEND="
	!!www-client/sputnik-browser-beta
	app-accessibility/at-spi2-atk:2
	app-accessibility/at-spi2-core:2
	app-misc/ca-certificates
	dev-libs/atk
	dev-libs/expat
	dev-libs/glib:2
	dev-libs/nspr
	dev-libs/nss
	media-fonts/liberation-fonts
	media-libs/alsa-lib
	media-libs/mesa[gbm(+)]
	media-libs/vulkan-loader
	net-misc/wget
	net-print/cups
	sys-apps/dbus
	sys-apps/util-linux
	sys-libs/glibc
	virtual/libudev
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:3[X]
	x11-libs/libdrm
	x11-libs/libX11
	x11-libs/libxcb
	x11-libs/libXcomposite
	x11-libs/libXcursor
	x11-libs/libXdamage
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXi
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libXScrnSaver
	x11-libs/libXtst
	x11-libs/pango
	x11-misc/xdg-utils
	x11-libs/libxkbcommon
	x11-libs/libxkbfile
"

DEPEND="
	>=dev-util/patchelf-0.9
"

QA_PREBUILT="*"
QA_DESKTOP_FILE="usr/share/applications/.*\\.desktop"
S=${WORKDIR}
BROWSER_HOME="opt/${MY_PN}"

pkg_setup() {
	chromium_suid_sandbox_check_kernel_config
}

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	rm -r etc || die
	rm -r lib || die
	rm -r "opt/cprocsp" || die

	gzip -d usr/share/doc/${PN}/changelog.gz || die
	gzip -d usr/share/man/man1/${PN}.1.gz || die
	if [[ -L usr/share/man/man1/${PN}.1.gz ]]; then
		rm usr/share/man/man1/${PN}.1.gz || die
		dosym ${PN}.1 usr/share/man/man1/${PN}.1
	fi

	# Remove symbolic link
	if [[ -L usr/share/man/man1/${MY_PN}.1.gz ]]; then
	        rm usr/share/man/man1/${MY_PN}.1.gz || die
	fi

        mv usr/share/doc/${PN} usr/share/doc/${PN}-${PVR} || die

	mv usr/share/appdata usr/share/metainfo

        pushd "${BROWSER_HOME}/locales" > /dev/null || die
        chromium_remove_language_paks
        popd > /dev/null || die

	# Исправляем иконку у ярлыка запуска
	sed -i -E -e "s/^Icon=sputnik-browser/Icon=${PN}/" usr/share/applications/*.desktop

	# Добавляем флаг запуска --no-sandbox, иначе не работает, т.к. требует древнюю версию glibc
	sed -i -E -e "s/^Exec=\/opt\/sputnik-browser\/sputnik-browser/Exec=\/opt\/sputnik-browser\/sputnik-browser --no-sandbox/" usr/share/applications/*.desktop

	default

	patchelf --remove-rpath "${S}/${BROWSER_HOME}/chrome-sandbox" || die "Failed to fix library rpath (chrome-sandbox)"
	patchelf --remove-rpath "${S}/${BROWSER_HOME}/sputnik_client" || die "Failed to fix library rpath (sputnik_client)"
}

src_install() {
	mv * "${D}" || die

	keepdir "${EPREFIX}/${BROWSER_HOME}"
	for icon in "${D}/${BROWSER_HOME}/product_logo_"*.png; do
		size="${icon##*/product_logo_}"
		size=${size%.png}
		dodir "/usr/share/icons/hicolor/${size}x${size}/apps"
		newicon -s ${size} "$icon" ${PN}.png
	done

	fperms 4711 "${EPREFIX}/${BROWSER_HOME}/print_screen_process"
}

pkg_postinst() {
	xdg_desktop_database_update
#	chmod 5777 "/${BROWSER_HOME}/print_screen_process"
#	chmod 4755 "${EPREFIX}/${BROWSER_HOME}/sputnik_client"
#	chmod 4755 "${EPREFIX}/${BROWSER_HOME}/chrome-sandbox"
#	chmod 4755 "${EPREFIX}/${BROWSER_HOME}/certificate_installer"

	if [ -f "/opt/${MY_PN}/params.dat" ] && [ -f "/opt/${MY_PN}/config.dat" ] && [ -f /opt/${MY_PN}/sputnik_client ]; then
		/opt/${MY_PN}/sputnik_client --generate_branding & wait
	fi
}

pkg_postrm() {
	xdg_desktop_database_update
}
