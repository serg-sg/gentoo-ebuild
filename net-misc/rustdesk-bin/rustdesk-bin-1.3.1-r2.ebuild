# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

PYTHON_COMPAT=( python3_{10..13} )
inherit xdg unpacker python-r1

MY_PN=${PN/-bin}
MY_PV=$(ver_cut 1-3)
MY_PV2=$(ver_rs 3 '-')

DESCRIPTION="A remote control software."
HOMEPAGE="https://rustdesk.com/"

SRC_URI="
	amd64? (
		https://github.com/${MY_PN}/${MY_PN}/releases/download/${MY_PV}/${MY_PN}-${MY_PV}-x86_64.deb -> ${P}.deb
	)
"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"

# Зависимости для запуска этой программы
RDEPEND="
	!net-misc/rustdesk
	dev-lang/python
	dev-libs/libappindicator
	net-misc/curl
	media-libs/alsa-lib
	media-libs/gstreamer
	media-libs/gst-plugins-base
	media-libs/libva[X]
	media-video/pipewire
	sys-apps/systemd-utils
	sys-libs/pam
	x11-libs/gtk+:3
	x11-libs/libxcb
	x11-libs/libvdpau
	x11-libs/libXfixes
	x11-misc/xdotool
	${PYTHON_DEPS}
"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RESTRICT="bindist mirror strip"

S=${WORKDIR}

src_unpack() {
	unpack_deb ${A}
}

src_install() {
	mv * "${D}" || die

	ln -s "/usr/lib/rustdesk/${MY_PN}" "${D}/usr/bin/${MY_PN}"

	# Добавление службы rustdesk
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')

	if [ "systemd" == "${INITSYS}" ]; then
		mkdir -p "${D}/usr/lib/systemd/system"
		cp "${D}/usr/share/${MY_PN}/files/systemd/${MY_PN}.service" "${D}/usr/lib/systemd/system/${MY_PN}.service"

		# try fix error in Ubuntu 18.04
                # Failed to reload rustdesk.service: Unit rustdesk.service is not loaded properly: Exec format error.
                # /usr/lib/systemd/system/rustdesk.service:10: Executable path is not absolute: pkill -f "rustdesk --"
                if [ -e /usr/bin/pkill ]; then
                        sed -i "s|pkill|/usr/bin/pkill|g" "${D}/usr/lib/systemd/system/rustdesk.service"
                fi
	else
		mkdir -p "${D}/etc/init.d/"

		echo '#!/sbin/openrc-run

name="RustDesk"
description="RustDesk Daemon Service"
supervisor="supervise-daemon"
command="/usr/bin/rustdesk"
command_args="--service"
command_user="root"
pidfile="/run/rustdesk"

depend() {
	after display-manager
	need net
}' > "${D}/etc/init.d/${MY_PN}"

	# Исправление прав на запуск службы
	chmod +x "${D}/etc/init.d/${MY_PN}"

	fi
}

pkg_preinst() {
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')
	if [ "systemd" == "${INITSYS}" ]; then
		service ${MY_PN} stop || true

		if [ -e /etc/systemd/system/${MY_PN}.service ]; then
			rm -f "/etc/systemd/system/${MY_PN}.service" "/usr/lib/systemd/system/${MY_PN}.service" \
				"/usr/lib/systemd/user/${MY_PN}.service"
		fi
	else
		rc-service ${MY_PN} stop
	fi
}

pkg_postinst() {
	# Добавление службы в автозагрузку и его запуск
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')
	if [ "systemd" == "${INITSYS}" ]; then
		systemctl daemon-reload
		systemctl enable ${MY_PN}
		systemctl start ${MY_PN}
	else
	# Исправление прав на запуск службы
	chmod +x "/etc/init.d/${MY_PN}"
		rc-update add ${MY_PN} default
		/etc/init.d/${MY_PN} start
	fi
}

pkg_prerm() {
	# Останавливаем службу и убираем из автозагрузки
	INITSYS=$(ls -al /proc/1/exe | awk -F' ' '{print $NF}' | awk -F'/' '{print $NF}')
	if [ "systemd" == "${INITSYS}" ]; then
		systemctl stop ${MY_PN} || true
		systemctl disable ${MY_PN} || true
		rm -f "/etc/systemd/system/${MY_PN}.service" "/usr/lib/systemd/system/${MY_PN}.service" \
			"/usr/lib/systemd/user/${MY_PN}.service" "/etc/sudoers.d/${MY_PN}"
	else
		rc-update delete ${MY_PN}
		rc-service ${MY_PN} stop
		rm -f "/etc/init.d/${MY_PN}" || die
	fi
}
