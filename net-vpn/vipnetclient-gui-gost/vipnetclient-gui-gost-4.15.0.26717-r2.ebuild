# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit desktop unpacker xdg

DESCRIPTION="ViPNet Client for Linux GUI by JSC InfoTeCS. GUI version"
HOMEPAGE="https://www.vipnet.ru"

MY_PV=${PV/0./0-}
MY_PN=${PN/-gost/_gost}

SRC_URI="
	amd64? ( https://git.calculate-linux.org/serg-sg/sources/raw/branch/master/net-vpn/vipnetclient/${MY_PN}_ru_amd64_${MY_PV}.deb )
"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="bindist mirror strip"

RDEPEND="
	!net-vpn/vipnetclient-gui
	!net-vpn/vipnetclient
	!net-vpn/vipnetclient-gost
	x11-libs/libxkbcommon[X(+)]
	x11-libs/libxcb[xkb(+)]
	sys-process/procps
"
#dev-qt/qtgui[linuxfb(+),vnc(+)]

S=${WORKDIR}

pkg_setup() {

	# Sandbox Disabled

	UPGRADE_DIR="/tmp/vipnetclient"
	rm -rf ${UPGRADE_DIR}
	mkdir -p ${UPGRADE_DIR}

	# Creating file-markers on each existing vipnetclient process and send them USR1
	for pid in $(pgrep -U0 -x vipnetclient); do
		local user=$(grep -awz USER /proc/${pid}/environ 2>/dev/null | tr -d '\0' | cut -c 6-)
		[ -z "$user" ] && continue
		echo ${pid} > /tmp/vipnetclient/${user}
	done

	pkill -x --signal USR1 vipnetclient 2>/dev/null

}

src_unpack() {

	unpack_deb ${A}

	# Переместим папку systemd чтобы не попал под фильтр INSTALL-MASK
	mv -f ${S}/etc/systemd ${S}/var/lib/vipnet/systemd || die

}

src_install() {

	mv * "${D}" || die

	# Set owners
	chown root:root ${D}/usr/bin/vipnetclient 2>/dev/null
	chown root:root $(ls -1 ${D}/etc/init.d/*vipnetclient) 2>/dev/null
	chmod 755 $(ls -1 ${D}/etc/init.d/*vipnetclient) 2>/dev/null
	chown root:root ${D}/usr/bin/vipnetclient-gui 2>/dev/null
	chown root:root ${D}/usr/bin/vipnetclient-gui-bin 2>/dev/null

}

pkg_postinst() {

	# Sandbox Disabled

	# Копируем файлы службы для systemd, без них не работает
	mkdir -p /etc/systemd/system || die
	cp -rf /var/lib/vipnet/systemd/system/* /etc/systemd/system/ || die

	# update man db
	mandb >/dev/null 2>&1

	# for some distrs
	find /var/lib/vipnet -type d -exec chmod 755 {} \;

	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update

	# create service and init scripts
	if which systemctl >/dev/null 2>&1; then
		systemctl reset-failed
		systemctl stop vipnetclientdaemon*
		elog "Служба vipnetclientdaemon установлена"
	elif which update-rc.d >/dev/null 2>&1; then
		update-rc.d vipnetclient defaults 90 10 >/dev/null 2>&1 \
		&& elog "Служба vipnetclient установлена"
	elif which rc-update	>/dev/null 2>&1; then
		rc-update add vipnetclient default \
		&& elog "Служба vipnetclient установлена"
	fi

	# Set capability
	/usr/bin/vipnetclient --setcap --version

	# Restart each running instance of vipnetclient-gui
	pkill -f -USR1 vipnetclient-gui-bin 2>/dev/null

	# Update shared libraries paths
	#ldconfig 2>/dev/null
	which ldconfig >/dev/null 2>&1 && ldconfig 2>/dev/null
}

pkg_prerm ()  {

	# Размонтируем виртуальный диск
	umount /home/*/.vipnet/var/run
	umount /root/.vipnet/var/run

	#update man db
	mandb >/dev/null 2>&1

	# Update shared libraries paths
	which ldconfig >/dev/null 2>&1 && ldconfig 2>/dev/null

	# disable service and remove init scripts
	if which systemctl >/dev/null 2>&1; then
		systemctl disable vipnetclient >/dev/null 2>&1
		if [ -f /etc/systemd/system/vipnetclient_gui_login.service ]; then
			systemctl disable vipnetclient_gui_login >/dev/null 2>&1
		fi
	elif which update-rc.d >/dev/null 2>&1; then
		update-rc.d -f vipnetclient remove >/dev/null 2>&1
	elif which rc-update >/dev/null 2>&1; then
		rc-update del vipnetclient >/dev/null 2>&1
	fi

	# При новой установке REPLACING_VERSIONS = "", при новой установке pkg_postrm() не выполняется.
	# При удалении REPLACING_VERSIONS не вычисляется, этап pkg_postinst() не выполняется.
	# При удалении REPLACED_BY_VERSION = "", при новой установке pkg_postrm() не выполняется.
	# При обновлении или переустановки выполняются этапы pkg_postinst() и pkg_postrm()
	# и, соответственно, переменные REPLACING_VERSIONS и REPLACED_BY_VERSION имеют ненулевое значение.
	# Таким образом мы можем определить, что пакет устанавливается, обновляется, переустанавливается поверх или удаляется
	# и выстраивать соответствующую логику.

	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		GUI_NAME=vipnetclient-gui-bin
		pkill -f --signal INT ${GUI_NAME} 2>/dev/null

		# Удалим мусор при окончательном удалении
		elog "Пакет ${PN} окончательно удаляется"
		rm -rf /etc/systemd/system/vipnetclient*
		rm -f /etc/rc*/*vipnetclient >/dev/null 2>&1
		rm -f /etc/vipnet_token.conf
	fi

}

pkg_postrm () {

	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update

}
