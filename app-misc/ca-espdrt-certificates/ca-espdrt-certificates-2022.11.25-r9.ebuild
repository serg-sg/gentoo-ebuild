# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Установка корневого сертификата для проекта ЕСПД Цифровая экономика"
HOMEPAGE="https://espd.wifi.rt.ru/settings/cert-install"

SRC_URI="
https://espd.wifi.rt.ru/docs/ca-root.crt -> ${P}_root.crt
"

LICENSE=""
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~loong ~m68k ~mips ppc ppc64 ~riscv ~s390 sparc x86 ~x64-cygwin ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris ~x86-winnt"
IUSE=""

# Зависимости
RDEPEND="
	app-misc/ca-certificates
"

RESTRICT="bindist mirror strip"

S="${WORKDIR}"

CA=usr/local/share/ca-certificates
ESPD_TR_R=${P}_root.crt

ESPD_TR_R_NAME="CA Root Social Objects - PJSC \"Rostelecom\""

## Ассоциативный массив {имя сертификата:файл}
declare -A massiv
massiv[${ESPD_TR_R_NAME}]=${ESPD_TR_R}

src_prepare() {
	mkdir -p ${CA} || die
	cp ${DISTDIR}/${A} ${CA} || die
	default
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	/usr/sbin/update-ca-certificates && \
	elog Установка корневого сертификата в /etc/ssl/certs/ выполнена

	# Установка для браузеров
	# Создаём базу nssdb для системы
	mkdir -p /etc/pki/nssdb || die

	nssdir=$(find /home/ -name "cert9.db")
	nssdir+=$'\n/etc/pki/nssdb/cert9.db'

	for certDB in ${nssdir}
	do
		certdir=$(dirname ${certDB});

		for k in "${!massiv[@]}"; do
			certutil -A -n "${k}" -t "CTu,Cu,Cu" -i /${CA}/${massiv[$k]} -d sql:${certdir} && \
			elog Установка корневого сертификата \"${k}\" в ${certdir} выполнена
		done
	done

        if [[ -n "$(find -L /etc/ssl/certs/ -type l)" ]] ; then
                ewarn "Следующие неработающие символические ссылки удалены:"
                ewarn "$(find -L /etc/ssl/certs/ -type l -printf '%p -> %l\n' -delete)"
        fi
}

pkg_prerm() {
	# Удаляем сертификаты из nss
	nssdir=$(find /home/ -name "cert9.db")
	nssdir+=$'\n/etc/pki/nssdb/cert9.db'

	for certDB in ${nssdir}
	do
		certdir=$(dirname ${certDB});

		for k in "${!massiv[@]}"; do
			certutil -D -n "${k}" -d sql:${certdir} && \
			elog Удаление корневого сертификата \"${k}\" из ${certdir} выполнено
		done
	done
}

pkg_postrm() {
	/usr/sbin/update-ca-certificates --fresh && \
	elog Удаление корневого сертификата из /etc/ssl/certs/ выполнено
}
