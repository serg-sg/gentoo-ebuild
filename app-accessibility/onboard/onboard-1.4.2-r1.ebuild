# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
DISTUTILS_EXT=1
PYTHON_COMPAT=( python3_{12..13} )

inherit distutils-r1 gnome2-utils

DESCRIPTION="An onscreen keyboard useful for tablet PC users and for mobility impaired users"
HOMEPAGE="https://launchpad.net/onboard"
SRC_URI="https://github.com/dr-ni/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+ BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm64"
IUSE="+accessibility debug"

COMMON_DEPEND="app-text/hunspell
	dev-libs/dbus-glib
	dev-python/dbus-python[${PYTHON_USEDEP}]
	dev-python/pycairo[${PYTHON_USEDEP}]
	dev-python/pygobject:3[${PYTHON_USEDEP}]
	gnome-base/dconf
	gnome-base/gsettings-desktop-schemas
	gnome-base/librsvg
	media-libs/libcanberra
	sys-apps/dbus
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:3[introspection]
	x11-libs/libX11
	x11-libs/libXi
	x11-libs/libXtst
	x11-libs/libwnck:3
	x11-libs/pango"

DEPEND="${COMMON_DEPEND}
	accessibility? (
		app-accessibility/at-spi2-core
		gnome-extra/mousetweaks
	)
	app-text/iso-codes
	dev-python/pyatspi[${PYTHON_USEDEP}]
	dev-util/intltool
	gnome-extra/yelp
	sys-power/acpid
	x11-libs/libxkbfile
"

BDEPEND="${COMMON_DEPEND}
	dev-python/python-distutils-extra[${PYTHON_USEDEP}]
"

RESTRICT="mirror test"

src_prepare() {
	default
	# Patch documentation paths
	einfo "Patch documentation path in ${S}/setup.py"
	sed -i 's:share/doc/onboard:share/doc/'${P}':g' "${S}/setup.py"

	distutils-r1_src_prepare
	eapply_user

	cp -f "${FILESDIR}/"*.po "${S}/po/"
}

src_install() {
	distutils-r1_src_install
}

pkg_preinst() {
	gnome2_icon_savelist
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
	gnome2_schemas_update
	xdg_desktop_database_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	gnome2_schemas_update
	xdg_desktop_database_update
}
