# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg unpacker

DESCRIPTION="MyOffice Standard. Home Edition is a set of office applications for creating text and spreadsheet documents."
HOMEPAGE="https://myoffice.ru/products/standard-home-edition/"
LICENSE="MyOffice"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist mirror strip"

if [[ ${PN} == myoffice-standard-home-edition ]] ; then
	MY_PN="myoffice-standard-home-edition"
fi

SRC_URI="https://preset.myoffice-app.ru/${MY_PN}_${PV}_amd64.deb"

RDEPEND="
	>=sys-apps/sed-1.1
	>=sys-apps/gawk-1.1
	>=sys-libs/glibc-2.17
	>=x11-libs/libxcb-1.13[xkb(+)]
	>=x11-misc/xdg-utils-1.1
"

S=${WORKDIR}

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	# Копируем mime
	path_mime=usr/share/mime/application
	mkdir -p ${path_mime}
	cp "${S}/opt/${MY_PN}/"*.xml ${path_mime} || die

	# Исправляем строки запуска в ярлыках запуска
	path_desktop=usr/share/applications
	echo Path=/opt/${MY_PN} >> ${path_desktop}/myoffice*.desktop

	# Перемещаем иконки приложений в формате svg
	path_icon=usr/share/icons/hicolor/scalable/apps
	mkdir -p ${path_icon}
	mv "${S}/opt/${MY_PN}/icons/app_text_icon.svg" ${path_icon}/myoffice-text-home-edition.svg || die
	mv "${S}/opt/${MY_PN}/icons/app_sheet_icon.svg" ${path_icon}/myoffice-spreadsheet-home-edition.svg || die

	APPS="text sheet"
	for app in ${APPS}; do
	echo app = $app

		for icon in "${S}/usr/share/icons/hicolor/"*; do
			size="${icon##*/hicolor/}"
			size="${size##*x}"
			path="usr/share/icons/hicolor/${size}x${size}/apps"
			mkdir -p $path
			if [ ${size} = "scalable" ]; then
				echo Пропускаем scalable
			elif [ ${app} = "text" ]; then
				mv "${icon}/app_${app}_icon-home-edition_"*.png "${path}/myoffice-${app}-home-edition.png" || die
			elif [ ${app} = "sheet" ]; then
				mv "${icon}/app_${app}_icon-home-edition_"*.png "${path}/myoffice-spread${app}-home-edition.png" || die
			fi
		done
	done

	rm -rf usr/share/icons/hicolor/*/editor_*.png

	default
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
	vxdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
