# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit unpacker xdg

DESCRIPTION="R7-Grafika desktop app"
HOMEPAGE="https://r7-office.ru/grafika"
LICENSE="Apache-2.0"
SLOT="0"

KEYWORDS="~amd64"
IUSE=""
RESTRICT="bindist strip mirror"

MY_PV=$(ver_rs 3- '')

SRC_URI="
	amd64? ( https://download.r7-office.ru/r7graph/${PN}-amd64-${MY_PV}.deb )
"

RDEPEND="
	x11-libs/gtk+:3
	x11-libs/libnotify
	dev-libs/nss
	x11-libs/libXScrnSaver
	x11-libs/libXtst
	x11-misc/xdg-utils
	app-accessibility/at-spi2-core
	sys-apps/util-linux
	app-crypt/libsecret
	dev-libs/libappindicator
"

S=${WORKDIR}
R7="opt/${PN}"

src_unpack() {
	unpack_deb ${A}
}

src_install() {
	mv * "${D}" || die
}

pkg_postinst() {
	if type update-alternatives 2>/dev/null >&1; then
		# Remove previous link if it doesn't use update-alternatives
		if [ -L '/usr/bin/R7Grafika' -a -e '/usr/bin/R7Grafika' -a "`readlink '/usr/bin/R7Grafika'`" \
			!= '/etc/alternatives/R7Grafika' ]; then
		rm -f '/usr/bin/R7Grafika'
		fi
		update-alternatives --install '/usr/bin/R7Grafika' 'R7Grafika' '/opt/R7Grafika/R7Grafika' 100
	else
		ln -sf '/opt/R7Grafika/R7Grafika' '/usr/bin/R7Grafika'
	fi

	# SUID chrome-sandbox for Electron 5+
	chmod 4755 '/opt/R7Grafika/chrome-sandbox' || die

	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		# Delete the link to the binary
		if type update-alternatives >/dev/null 2>&1; then
			update-alternatives --remove 'R7Grafika' '/usr/bin/R7Grafika'
		else
			rm -f '/usr/bin/R7Grafika'
		fi
	fi
	xdg_desktop_database_update
}
