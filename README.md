# gentoo-ebuild

Сборник своих ebuild для Gentoo/Calculate Linux

## Добавление данного репозитория

    # eselect repository add serg-sg git https://git.calculate-linux.org/serg-sg/gentoo-ebuild.git

    Если при добавлении репозитория на предыдущем шаге он не включился, то включить можно следующей командой:

    # eselect repository enable serg-sg

    Далее необходимо синхронизировать базы emerge и eix:
    
    # emerge --sync serg-sg
    # eix-update

### Ручное добавление

Создайте файл /etc/portage/repos.conf/serg-sg.conf:

    [serg-sg]
    location = /var/db/repos/serg-sg
    sync-uri = https://git.calculate-linux.org/serg-sg/gentoo-ebuild.git
    sync-type = git
    auto-sync = yes

После добавления репозитория выполните синхронизацию следующими способами:

    # emaint sync --repo serg-sg

или

    # emerge --sync

или

    # eix-sync

В Calculate-Linux:

    cl-update --update-other on --sync

## Удаление данного репозитория

    # eselect repository remove -f serg-sg
    # eix-update

## Частые вопросы по portage

### Размаскировка по архитектуре/keywords

Бывают пакеты с маскированием по keywords. Чтобы установить такие пакеты необходимо либо добавить репозиторий в размаскирование по keywords или размаскировать только необходимый пакет.

Пример размаскирования/разблокировки по архитектуре всего репозитория:

    echo "*/*::serg-sg ~amd64" >> /etc/portage/package.accept_keywords/custom

Пример размаскирования/разблокировки по архитектуре с указанием категории и имени пакета, репозитория и без его версии:

    echo "www-client/yandex-browser-stable::serg-sg ~amd64" >> /etc/portage/package.accept_keywords/custom

### Маскирование/блокировка пакета/репозитория

В случае, если необходимо заблокировать установку установку из репозитория всех пакетов или определённого пакета, необходимо внести необходимые записи в файл **/etc/portage/package.mask/custom**.

Пример маскирования/блокировки всего репозитория:

    echo "*/*::serg-sg ~amd64" >> /etc/portage/package.mask/custom

Пример маскирования/блокировки с указанием категории и имени пакета, репозитория и без его версии:

    echo "www-client/yandex-browser-stable::serg-sg" >> /etc/portage/package.mask/custom
