# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit desktop unpacker pax-utils xdg

DESCRIPTION="R7-Office Desktop Editors is an application for editing office documents"
HOMEPAGE="https://r7-office.ru/"
LICENSE="AGPLv3"
SLOT="0"

KEYWORDS="amd64"
IUSE=""
RESTRICT="bindist strip mirror"

SRC_URI="
	amd64? ( https://download.r7-office.ru/ubuntu/${PN}_${PV}-159_stretch_amd64_Debian_Based_Astra_Ubuntu.deb -> ${P}.deb )
"

RDEPEND="
	x11-base/xorg-server
	media-plugins/alsa-plugins

	|| (
		net-misc/curl
		net-misc/wget
	)

	media-plugins/gst-plugins-libav
	media-libs/gst-plugins-ugly
	app-accessibility/at-spi2-core
	app-accessibility/at-spi2-atk
	x11-libs/gtk+:3
	x11-libs/cairo
	x11-misc/xdg-utils

	media-fonts/dejavu
	media-fonts/liberation-fonts
	media-fonts/crosextrafonts-carlito
	media-fonts/corefonts
	media-fonts/takao-fonts
"

S=${WORKDIR}
R7="opt/${PN}"

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	mkdir -p usr/share/mime/application
	sed -i 's/version="1.0"?>/version="1.0" encoding="utf-8"?>/' ${R7}/desktopeditors/mimetypes/*.xml
	cp -r ${R7}/desktopeditors/mimetypes/*.xml usr/share/mime/application

	# Исправляем ссылки на иконки
	sed -i -E -e 's/^Icon='${PN}'/Icon=\/opt\/'${PN}'\/mediaviewer\/ivapp.ico/' usr/share/applications/${PN}-imageviewer.desktop
	sed -i -E -e 's/^Icon='${PN}'/Icon=\/opt\/'${PN}'\/mediaviewer\/mvapp.ico/' usr/share/applications/${PN}-videoplayer.desktop

	default
}

src_install() {
	mv * "${D}" || die

	for icon in "${D}/${R7}/desktopeditors/asc-de-"*.png; do
		size="${icon##*/asc-de-}"
		size=${size%.png}
		dodir "/usr/share/icons/hicolor/${size}x${size}/apps"
		newicon -s ${size} "$icon" ${PN}.png
	done
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
