# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg

DESCRIPTION="Dr.Web for Linux Workstations"
HOMEPAGE="https://www.drweb.ru/"

SRC_URI="

        x86? (
		http://cdn-download.drweb.com/pub/drweb/unix/workstation/11.1/drweb-${PV}-av-linux-x86.run
	)
	amd64? (
		http://cdn-download.drweb.com/pub/drweb/unix/workstation/11.1/drweb-${PV}-av-linux-amd64.run
	)
"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="bindist strip mirror"

S=${WORKDIR}

pkg_preinst() {

	# Sandbox	Disabled
	chmod a+x "${FILESDIR}/drweb-configd" || die
	chmod a+x "${DISTDIR}/${A}" || die

	"${DISTDIR}/${A}" --tar xfC ${S} || die

	# Запускаем установку
	${S}/install.sh --non-interactive

}

pkg_postinst() {

	# Sandbox	Disabled

	# Заменяем сценарий запуска для работы с open-rc
	if which rc-service >/dev/null 2>&1; then
		cp -f "${FILESDIR}/drweb-configd" /etc/init.d/ && elog "Служба drweb-configd для Open-RC скопирована."
	fi

	# Добавляем службы в автозагрузку
	if which systemctl >/dev/null 2>&1; then
		systemctl enable drweb-configd >/dev/null 2>&1 && elog "Служба drweb-configd установлена."

	elif which update-rc.d >/dev/null 2>&1; then
		update-rc.d drweb-configd defaults 90 10 >/dev/null 2>&1 && elog "Служба drweb-configd установлена."

	elif which rc-update >/dev/null 2>&1; then
		rc-update add drweb-configd default >/dev/null 2>&1 && elog "Служба drweb-configd установлена."
	fi

	[ -x /etc/init.d/drweb-configd ] && /etc/init.d/drweb-configd start

	xdg_desktop_database_update
	xdg_icon_cache_update

}

pkg_prerm() {

	if [ "${REPLACED_BY_VERSION}" = "" ]; then
		elog "Пакет окончательно удаляется"
		/opt/drweb.com/bin/remove.sh --non-interactive || die
		elog "Если желаете, то можете вручную удалить старые данные командой:"
		elog "# rm -rf /var/opt/drweb.com"

		# Удаляем службы
		if which systemctl >/dev/null 2>&1; then
			systemctl disable drweb-configd >/dev/null 2>&1
		elif which update-rc.d >/dev/null 2>&1; then
			update-rc.d -f drweb-configd remove >/dev/null 2>&1
		elif which rc-update >/dev/null 2>&1; then
			rc-update del drweb-configd >/dev/null 2>&1
		fi
	fi

}

pkg_postrm() {

	xdg_desktop_database_update
	xdg_icon_cache_update

}
