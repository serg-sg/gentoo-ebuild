# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop unpacker xdg

DESCRIPTION="ONLYOFFICE Desktop Editors is an application for editing office documents (text documents, spreadsheets and presentations) from ONLYOFFICE cloud portal on local computer without browser using"
HOMEPAGE="https://www.onlyoffice.com/"
SRC_URI="
	amd64? (
		https://github.com/ONLYOFFICE/DesktopEditors/releases/download/v"${PV}"/onlyoffice-desktopeditors_amd64.deb \
			-> "${P}"_amd64.deb
	)
"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="help"
RESTRICT="bindist strip mirror"

RDEPEND="
	!app-office/onlyoffice

	|| (
		net-misc/curl
		net-misc/wget
	)

	media-plugins/gst-plugins-libav
	media-libs/alsa-lib
	media-libs/gst-plugins-ugly

	dev-libs/atk
	sys-devel/gcc
	x11-base/xorg-server

	x11-libs/gtk+:3
	x11-libs/cairo
	x11-libs/libXScrnSaver
	x11-misc/xdg-utils

	media-fonts/dejavu
	media-fonts/liberation-fonts
	media-fonts/crosextrafonts-carlito
	media-fonts/corefonts
	media-fonts/takao-fonts

	help? ( app-office/onlyoffice-help-bin )
"

S=${WORKDIR}
MY_P="onlyoffice"
OPT="opt/${MY_P}"

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	gzip -d usr/share/doc/${MY_P}-desktopeditors/changelog.Debian.gz || die
	mkdir -p usr/share/doc/${PF}
	mv usr/share/doc/${MY_P}-desktopeditors/changelog.Debian usr/share/doc/${PF}/changelog || die
	rm -rf usr/share/doc/${MY_P}-desktopeditors

	mkdir -p usr/share/mime/application
	sed -i 's/version="1.0"?>/version="1.0" encoding="utf-8"?>/' ${OPT}/desktopeditors/mimetypes/*.xml
	cp -r ${OPT}/desktopeditors/mimetypes/*.xml usr/share/mime/application

	default
}

src_install() {
	mv * "${D}" || die

	for icon in "${D}/${OPT}/desktopeditors/asc-de-"*.png; do
		size="${icon##*/asc-de-}"
		size=${size%.png}
		dodir "/usr/share/icons/hicolor/${size}x${size}/apps"
		newicon -s ${size} "$icon" ${MY_P}-desktopeditors.png
	done
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
